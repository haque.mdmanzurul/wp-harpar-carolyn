<?php
/* Removing Tabs*/
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab

    return $tabs;

}
/*Adding Custom Tab*/
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
	// Adds the new tab
	$tabs['course_info'] = array(
	'title' => __( 'Course Info', 'woocommerce' ),
	'priority' => 50,
	'callback' => 'woo_new_product_tab_content_course_info'
	);
	
	$tabs['modules'] = array(
	'title' => __( 'Modules', 'woocommerce' ),
	'priority' => 55,
	'callback' => 'woo_new_product_tab_content_modules'
	);
	
	$tabs['assesment_criteria'] = array(
	'title' => __( 'How you are assesed', 'woocommerce' ),
	'priority' => 60,
	'callback' => 'woo_new_product_tab_content_assesment'
	);
	return $tabs;
}
//Course info Tab data
function woo_new_product_tab_content_course_info() {
	// The new tab content
	echo the_field('course_info');
} 


//Modules Tabs
function woo_new_product_tab_content_modules() {
	// The new tab content
	echo the_field('modules');
}

//Assesment tab
function woo_new_product_tab_content_assesment() {
	// The new tab content
	echo the_field('assesment');
}

//Remove Product Page sidebar
add_action('template_redirect', 'remove_sidebar_shop');
function remove_sidebar_shop() {
if ( is_singular('product') ) {
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar');
    }
}

/*
* wc_remove_related_products
*
* Clear the query arguments for related products so none show.
* Add this code to your theme functions.php file.
*/
function wc_remove_related_products( $args ) {
return array();
}
add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 

//get rpoduct taxonomies
function get_woo_taxonomies($taxonomy = 'joblevels', $start_title = "Select a Job Level"){
	$output = "<select name=\"$taxonomy\" class=\"turnintodropdown\"><option>$start_title</option>";
 
	$orderby      = 'name';  
	$show_count   = 1;      // 1 for yes, 0 for no
	$pad_counts   = 1;      // 1 for yes, 0 for no
	$hierarchical = 1;      // 1 for yes, 0 for no  
	$title        = '';  
	$empty        = 0;
	$args = array(
	  'taxonomy'     => $taxonomy,
	  'orderby'      => $orderby,
	  'show_count'   => $show_count,
	  'pad_counts'   => $pad_counts,
	  'hierarchical' => $hierarchical,
	  'title_li'     => $title,
	  'hide_empty'   => $empty
	); 

	$all_categories = get_categories( $args );
	foreach ($all_categories as $cat) {

		if($cat->category_parent == 0) {
			$category_id = $cat->term_id;
			$output .='<option value="'.$cat->term_id.'"><a href="#">'. $cat->name .'</a></li>'; 
		}  	   
		
	}
	$output .='</select>';	
	return $output;	
}

/* Get Woo-commerce Product*/
function get_products($atts){
    $a = shortcode_atts( array(
        'cats' =>'',
        'qualifications' => '',
		'joblevels' => '',
		'display_title'=>'courses'
    ), $atts );		
	
	global $paged;
    $curpage = $paged ? $paged : 1;
	$tax_query = array('relation' => 'AND');
	$cat = isset($_REQUEST['product_cat'])&&is_numeric($_REQUEST['product_cat'])?$_REQUEST['product_cat']:'';
	if($cat!=''){
	array_push($tax_query,array(
				'taxonomy' => 'product_cat',
				'field' => 'term_id',
				'terms' => $cat
			));
	}else if($a['cats']!=""){
	array_push($tax_query,array(
				'taxonomy' => 'product_cat',
				'field' => 'term_id',
				'terms' => explode(",",$a['cats'])
			));		
	}
			
	$joblevel = isset($_REQUEST['joblevels'])&&is_numeric($_REQUEST['joblevels'])?$_REQUEST['joblevels']:'';
	if($joblevel!=''){
	array_push($tax_query,array(
				'taxonomy' => 'joblevels',
				'field' => 'term_id',
				'terms' => $joblevel
			));
	}else if($a['joblevels']!=""){
	array_push($tax_query,array(
				'taxonomy' => 'joblevels',
				'field' => 'term_id',
				'terms' => explode(",",$a['joblevels'])
			));		
	}
	
	$qualifications = isset($_REQUEST['qualifications'])&&is_numeric($_REQUEST['qualifications'])?$_REQUEST['qualifications']:'';
	if($qualifications!=''){
		array_push($tax_query,array(
					'taxonomy' => 'qualifications',
					'field' => 'term_id',
					'terms' => $qualifications
				));	
	}else if($a['qualifications']!=""){
	array_push($tax_query,array(
				'taxonomy' => 'qualifications',
				'field' => 'term_id',
				'terms' => explode(",",$a['qualifications'])
			));		
	}
			
	$page = get_query_var( 'page' );								
	$args = array(
		'posts_per_page' => 4,
		'post_type' => 'product',
		'orderby' => 'title',
		'paged'=>$paged
	);
	
	if(count($tax_query)!=0)
	$args['tax_query'] = $tax_query;
	
	 $myquery = new WP_Query($args);
	 ob_start();
	 ?>
     
     <?php
	 if ( $myquery->have_posts() ) : 
      ?>
     <div class="itemcount">Displaying <?php if($myquery->found_posts<4) echo $myquery->found_posts; else if($myquery->found_posts<$args['paged']*4) echo $myquery->found_posts; else if($args['paged']==0) echo 4; else echo $args['paged']*4; ?> of <?php echo  $myquery->found_posts;  ?> <?php echo ($a['display_title']=="" OR $a['display_title']=="courses")?"courses":"ebooks" ?></div>
     <ul class="products custom-search">
     <?php 
	 while ( $myquery->have_posts() ) : $myquery->the_post(); global $product; 
	?>
	<li>
    <div class="product_image">
    <?php 
			//display product thumbnail
		if (has_post_thumbnail()) { 
		$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'thumbnail' ); 
		echo '<img src="' . $image_src[0] . '" width="140" alt="" />';
		}
		else {
		  echo '<img src="/images/defaul_image.jpg" width="140" alt=" />';
		}
		?>
    </div>
    <div class="product_description">
    	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php the_excerpt(); 
        	 $price = get_post_meta( get_the_ID(), '_regular_price', true); ?>	

        <div class="product-price"><span class="price">Price</span><span class="price-digit"><?php echo get_woocommerce_currency_symbol().' '.$price ?></span></div>
        
     
    <div class="product-meta">
    <a href="<?php the_permalink()?>" class="find-out-more" style="background-color:<?php echo get_field('readmore_buttom_background') ?>">Find out more</a>
    <a class="add_to_cart_button product_type_simple" style="background-color:<?php echo get_field('readmore_buttom_background') ?>" data-quantity="1" data-product_sku="<?php echo $product->get_sku(); ?>" data-product_id="<?php the_ID(); ?>" rel="nofollow" href="/course-finder/?add-to-cart=23">Buy Now</a>
    </div> 
    </div>  
	</li>
     <?php
	 endwhile; ?></ul>
      <?php 
	 wp_paginate($myquery); 
    echo '
    <div class="navigation"><ol class="wp-paginate">
        <li><a class="first page" href="'.get_pagenum_link(($curpage-1 > 0 ? $curpage-1 : 1)).'">&laquo;</a></li>';
        for($i=1;$i<=$myquery->max_num_pages;$i++)
            echo '<li><a class="'.($i == $curpage ? 'current ' : '').'page" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
        echo '
       <!-- <li><a class="page" href="'.get_pagenum_link(($curpage+1 <= $myquery->max_num_pages ? $curpage+1 : $myquery->max_num_pages)).'">&raquo;</a></li>--></ol>
    </div>
    ';  
	 else:
	 ?>
     <p>No Course found. Please search again</p>
     <?php
	 endif;
	 
	 $html = ob_get_clean();
	 
	 return $html;
}
add_shortcode('woocommerce_products','get_products');
/**
 * Add view button to product in best seeler tab 
 *
 */
function add_view_button_to_home_products(){
	
	echo  '<span class="view-product" style="background-color:'.get_field('readmore_buttom_background').'">View</span>';
	
}
add_action('woocommerce_before_shop_loop_item_title','add_view_button_to_home_products');


function vantage_metaslider_product_setting_metabox(){
	add_meta_box('vantage-metaslider-page-slider', __('Page Meta Slider', 'vantage'), 'vantage_metaslider_page_setting_metabox_render', 'product', 'side');
}
add_action('add_meta_boxes', 'vantage_metaslider_product_setting_metabox');


function woo_mini_cart_custom(){
 global $woocommerce;
 $output = "";
 ob_start();
?>
 <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item in the basket', '%d items in the basket', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></a> 
<?php
 $output = ob_get_clean();
 return $output; 	
}

add_shortcode("woo-custom-mini-cart","woo_mini_cart_custom");

?>