<?php
/**
 * Displays 
 * 
 * @package vantage
 * @since vantage 1.0
 * @license GPL 2.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

	<div class="entry-main">

		<?php do_action('vantage_entry_main_top') ?>

		<header class="entry-header">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'vantage' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			<?php if( has_post_thumbnail() && siteorigin_setting('blog_featured_image') ): ?>
				<div class="entry-thumbnail"><?php the_post_thumbnail( is_active_sidebar('sidebar-1') ? 'post-thumbnail' : 'vantage-thumbnail-no-sidebar' ) ?></div>
			<?php endif; ?>





		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php the_excerpt( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'vantage' ) ); ?>
		</div><!-- .entry-content -->
		<div class="meta-container">
			<?php if(vantage_get_post_categories()) : ?>
                <div class="entry-categories">
                    <span><i class="fa fa-calendar"></i>&nbsp;&nbsp;<?php the_date(); ?></span>&nbsp;&nbsp;<span><i class="fa fa-user"></i>&nbsp;&nbsp;<?php the_author(); ?></span><?php //echo vantage_get_post_categories() ?>
                </div>
            <?php endif; ?>
            <?php if ( get_post_type() == 'post' ) : ?>
                <div class="entry-meta">
                 <a href="<?php the_permalink() ?>" class="readmore">Continue reading</a>
                </div><!-- .entry-meta -->
            <?php endif; ?>
         </div>   
		<?php do_action('vantage_entry_main_bottom') ?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
