<?php
global $content_width, $vantage_site_width;

$vantage_site_width = 1170;

function vantage_child_setup() {
	global $content_width, $vantage_site_width;
	if ( ! isset( $content_width ) ) $content_width = 720; /* pixels */

	if ( ! isset( $vantage_site_width ) ) {
		$vantage_site_width = siteorigin_setting('layout_bound') == 'full' ? 1170 : 1010;
	}
	
	add_image_size('woo-cart-image', 150, 200, true);
}
add_action( 'after_setup_theme', 'vantage_child_setup' );
/**
 * Enqueue scripts and styles
 */
function vantage_child_scripts() {
	wp_deregister_style( 'vantage-fontawesome' );
	//wp_enqueue_style( 'vantage-bootstrap', get_bloginfo('url').'/wp-content/themes/vantagechild-harpar/css/bootstrap.min.css', array(), '3.2.0' );
	wp_enqueue_style( 'vantage-fontawesome', get_bloginfo('url').'/wp-content/themes/vantagechild-harpar/css/font-awesome.min.css', array(), '4.2.0' );
	
	//wp_enqueue_script( 'vantage-bootstrap' , get_bloginfo('url').'/wp-content/themes/vantagechild-harpar/js/bootstrap.min.js', array(), '1.0' );
	wp_enqueue_script( 'vantage-styledselect' , get_bloginfo('url').'/wp-content/themes/vantagechild-harpar/js/select.js', array(), '1.0' );
	wp_enqueue_script( 'vantage-styledselectcustom' , get_bloginfo('url').'/wp-content/themes/vantagechild-harpar/js/custom.js', array(), '1.0' );

}
add_action( 'wp_enqueue_scripts', 'vantage_child_scripts' );

/**
 * Register footer sidebar
 */
if ( function_exists('register_sidebar') ) {
register_sidebar(array(
'name' => 'Footer Secondary Sidebar',
'id' => 'sidebar-footer-secondary',
'description' => 'Appears as the sidebar on the custom homepage',
'before_widget' => '<div id="%1$s" class="widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h3 class="widgettitle">',
'after_title' => '</h3>',
));
register_sidebar(array(
'name' => 'Blog Sidebar',
'id' => 'sidebar-blog',
'description' => 'Appears as the sidebar on the Blog Page',
'before_widget' => '<div id="%1$s" class="widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h3 class="widgettitle">',
'after_title' => '</h3>',
));
}

/**
 * Get Woo-Commerce categories 
 * Usage Shortcode [woo-product-category]
 */  
function get_woocommerce_categories($atts){
    $a = shortcode_atts( array(
        'taxonomy' => 'product_cat',
        'bar' => 'something else',
    ), $atts );	
	$output = "<ul class=\"woocommerce-categories\">";
 
	$taxonomy     =  $a['taxonomy'];
	$orderby      = 'name';  
	$show_count   = 1;      // 1 for yes, 0 for no
	$pad_counts   = 1;      // 1 for yes, 0 for no
	$hierarchical = 1;      // 1 for yes, 0 for no  
	$title        = '';  
	$empty        = 0;
	$args = array(
	  'taxonomy'     => $taxonomy,
	  'orderby'      => $orderby,
	  'show_count'   => $show_count,
	  'pad_counts'   => $pad_counts,
	  'hierarchical' => $hierarchical,
	  'title_li'     => $title,
	  'hide_empty'   => $empty
	); 

	$all_categories = get_categories( $args );
	//print_r($all_categories);
	foreach ($all_categories as $cat) {
		//print_r($cat);
		if($cat->category_parent == 0) {
			$category_id = $cat->term_id;
			$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
			if($cat->name == "Ebooks")
			continue;
			
			$image = wp_get_attachment_url( $thumbnail_id );
			$output .='<li class="woo-category '.str_replace('amp;','-',str_replace(' ','-',$cat->name)).'"><div class="category-inner">';
			if ( $image ) {
				$output .='<img src="' . $image . '" alt="" />';
			} 		  
			$read_more_button_style = get_field('read_more_button_style', $taxonomy.'_'.$category_id);
			$product_counts = (($cat->count>0)?$cat->count:0).' '.(($cat->count>1)?" courses available":" course available");
			$output .='<br /><h4>'. $cat->name .'<span>'.$product_counts.'</span></h4><a class="readmore '.strtolower($read_more_button_style).'" href="'. get_bloginfo('url').'/course-finder/?product_cat='.$cat->term_id .'">View these courses</a>'; 

			$args2 = array(
			  'taxonomy'     => $taxonomy,
			  'child_of'     => 0,
			  'parent'       => $category_id,
			  'orderby'      => $orderby,
			  'show_count'   => $show_count,
			  'pad_counts'   => $pad_counts,
			  'hierarchical' => $hierarchical,
			  'title_li'     => $title,
			  'hide_empty'   => $empty
			);
			$sub_cats = get_categories( $args2 );
				if($sub_cats) {
					foreach($sub_cats as $sub_category) {
						echo  $sub_category->name ;
					}
		
				}
		}  
		$output .='</div></li>';   
		
	}	
	return $output;
}
add_shortcode('woo-product-category','get_woocommerce_categories'); 



/**
 * Get Woo-Commerce categories 
 * Usage Shortcode [woo-product-finder]
 */ 
function course_finder_search_form($atts){
    $a = shortcode_atts( array(
        'action' =>'',
        'class' => '',
    ), $atts );	
 	$output = "";	

	$output .='<div id="product-find-searchform" class="product-search-form" role="contentinfo">
				<div id="searchform" class="product-search-form">
					<form name="product-finder-form" action="'.$a['action'].'" method="get">';
	$joblevels = get_woo_taxonomies('joblevels','Select a Job Level');	
	$qualifications = get_woo_taxonomies('qualifications','Select a Qualification');
	$category = get_woo_taxonomies('product_cat','Select a Category');	
	$output .='<p class="field">'.$joblevels.'</p>';
	$output .='<p class="field">'.$qualifications.'</p>';
	$output .='<p class="field">'.$category.'</p>';                       
	$output .='<p class="field last"><button type="submit" name="submit">Search now <i class="fa fa-angle-right"></i></button></p></form></div></div>';  
	
	return $output; 

}
add_shortcode('woo-product-finder','course_finder_search_form');



/**
 * Render the slider.
 */
function vantage_child_render_slider(){

	if( is_front_page() && siteorigin_setting('home_slider') != 'none' ) {
		$settings_slider = siteorigin_setting('home_slider');

		if(!empty($settings_slider)) {
			$slider = $settings_slider;
		}
	}

	if( (is_page() or is_product('singular')) && get_post_meta(get_the_ID(), 'vantage_metaslider_slider', true) != 'none' ) {
		$page_slider = get_post_meta(get_the_ID(), 'vantage_metaslider_slider', true);
		if( !empty($page_slider) ) {
			$slider = $page_slider;
		}
	}


	if( empty($slider) ) return;

	global $vantage_is_main_slider;
	$vantage_is_main_slider = true;

	?><div id="main-slider" <?php if( siteorigin_setting('home_slider_stretch') ) echo 'data-stretch="true"' ?>><?php


	if($slider == 'demo') get_template_part('slider/demo');
	elseif( substr($slider, 0, 5) == 'meta:' ) {
		list($null, $slider_id) = explode(':', $slider);
		$slider_id = intval($slider_id);

		echo do_shortcode("[metaslider id=" . $slider_id . "]");
	}

	?></div><?php
	$vantage_is_main_slider = false;
}


/**
 * Secondary footer widget area
 */ 
function secondary_footer(){
?>
<div id="secondary-footer" class="site-footer-secondary" role="contentinfo">

	<div id="footer-widgets" class="full-container">
		<?php dynamic_sidebar( 'sidebar-footer-secondary' ) ?>
	</div><!-- #footer-widgets -->
</div>    
<?php	
}
add_action( 'vantage_before_footer','secondary_footer' );

function add_google_language_meta(){
	echo '<meta name="google-translate-customization" content="7757ec27e93bb10c-5e90a2db81b96705-g3788bafd0728af8b-a"></meta>';	
}
add_action( 'wp_head','add_google_language_meta' );

function lightbox_form($atts){
    $a = shortcode_atts( array(
        'form_id' =>'',
        'title' => '',
		'target_class'=>'',
		'target_link'=>''
    ), $atts );	
	
	ob_start();
	echo '<div style="display:none">
    <div id="'.$a['target_class'].'" style="padding: 10px;background: #fff">';
	 echo do_shortcode('[contact-form-7 id="'.$a['form_id'].'" title="'.$a['form_id'].'"]');
     echo '</div></div>';	 
	 $output = ob_get_clean();
	 return $output; 	 
	 	
}

add_shortcode('lightbox_form','lightbox_form');

include_once("inc/general_functions.php");
include_once("inc/class-featured-image-metabox-cusomizer.php");

new Featured_Image_Metabox_Customizer(array(
    'post_type'     => 'product',
    'metabox_title' => __( 'Product Image', 'woocommerce' ),
    'set_text'      => __( 'Set Product Image', 'woocommerce' ),
    'remove_text'   => __( 'Remove Product Image', 'woocommerce' )
));

function floating_enquire_button(){
	echo  '<a class="floating-enquire-icon lbp-inline-link-1" href="#"><img src="'.get_bloginfo('url').'/wp-content/themes/vantagechild-harpar/images/icon-enquirynow.png"/></span>';
	
}
add_action('wp_footer','floating_enquire_button');
?>

