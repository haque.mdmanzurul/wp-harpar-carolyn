<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product,$post;
?>
<div class="product-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price">Price<br/><?php echo $product->get_price_html(); ?><a href="#" rel="nofollow" data-product_id="<?php echo $post->ID; ?>" data-product_sku="<?php echo $product->sku; ?>" data-quantity="1" class="add_to_cart_button product_type_simple">Buy Now</a></p>
	
	<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>